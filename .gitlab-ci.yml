image: ubuntu:latest


# This template should be usable on any system that's based on apt.
# taken from tor gitlabci

.apt-template: &apt-template |
      export LC_ALL=C.UTF-8
      echo Etc/UTC > /etc/timezone
      mkdir -p apt-cache
      export APT_CACHE_DIR="$(pwd)/apt-cache"
      echo 'quiet "1";' \
           'APT::Install-Recommends "0";' \
           'APT::Install-Suggests "0";' \
           'APT::Acquire::Retries "20";' \
           'APT::Get::Assume-Yes "true";' \
           'Dpkg::Use-Pty "0";' \
           "Dir::Cache::Archives \"${APT_CACHE_DIR}\"; " \
        >> /etc/apt/apt.conf.d/99gitlab
      apt-get update -qq
      apt-get upgrade -qy

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

workflow:
  rules:
    - if: $CI_PROJECT_NAME == "support"
      variables:
        TRANSLATION_BRANCH: "support-portal"
    - if: $CI_PROJECT_NAME == "manual"
      variables:
        TRANSLATION_BRANCH: "tbmanual-contentspot"
    - if: $CI_PROJECT_NAME == "community"
      variables:
        TRANSLATION_BRANCH: "communitytpo-contentspot"
    - if: $CI_PROJECT_NAME == "tpo"
      variables:
        TRANSLATION_BRANCH: "tpo-web"
    - if: $CI_PROJECT_NAME == "gettor-web"
      variables:
        TRANSLATION_BRANCH: "gettor-website-contentspot"

stages:
    - build
    - test_l10n

pages:
  cache:
    key: $CI_PROJECT_PATH_SLUG.${CI_COMMIT_REF_SLUG}
    paths:
      - packages
      - lego
      - apt-cache
      - venv
      - .cache/pip
      - .cache/lektor/builds/
  stage: build
  script:
    - *apt-template
    - DEBIAN_FRONTEND=noninteractive apt-get install gettext python3-babel python3-pip git python3-inifile python3-dev python3-setuptools python3-openssl python3-cryptography python3-venv i18nspector apt-utils ca-certificates -y
    - python3 -m venv venv
    - source venv/bin/activate
    - pip3 install lektor
    - echo 'checking out translations'
    - rm -rf i18n
    - git clone --branch $TRANSLATION_BRANCH --depth=1 https://git.torproject.org/translation.git i18n
    - echo 'reinstall lektor plugins'
    - lektor project-info --output-path
    - lektor plugins reinstall
    - echo 'building lektor 3 more times to get translations in place'
    - lektor build --output-path public && lektor build --output-path public && lektor build --output-path public
  artifacts:
    paths:
      - public
      - i18n
  rules:
    - when: always

check_new_strings:
  cache:
    key: $CI_PROJECT_PATH_SLUG.${CI_COMMIT_REF_SLUG}
    paths:
      - packages
      - lego
      - apt-cache
      - venv
      - .cache/pip

  stage: test_l10n
  needs: [pages]
  allow_failure: true
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $TRANSLATION_BRANCH
    - if: '$CI_COMMIT_BRANCH == "translations"'
      when: never
  script:
    - *apt-template
    - DEBIAN_FRONTEND=noninteractive apt-get install gettext git python3-dev python3-setuptools i18nspector python3-polib python3-requests ca-certificates apt-utils -y
    - git clone https://gitlab.torproject.org/tpo/community/l10n.git
    - echo 'lets see if there are any updates in the strings for translation'
    - l10n/bin/check_po_status.py $TRANSLATION_BRANCH
  artifacts:
    paths:
      - public
      - i18n
      - l10n

check_l10n:
  allow_failure: true
  cache:
    key: $CI_PROJECT_PATH_SLUG.${CI_COMMIT_REF_SLUG}
    paths:
      - packages
      - lego
      - apt-cache
      - venv
      - i18n
      - .cache/pip
  stage: test_l10n
  needs: [pages]
  only:
    - translations
  script:
    - DEBIAN_FRONTEND=noninteractive apt-get install gettext i18nspector python3-polib ca-certificates -y
    - echo 'lets see if there are any broken links on the translations'
    - rm -rf l10n
    - git clone https://gitlab.torproject.org/tpo/community/l10n.git
    - l10n/bin/check_markdown_links.py i18n/

